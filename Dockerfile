# do not use https://hub.docker.com/_/python as there can be an error when building python module binaries without glibc
FROM alpine

MAINTAINER Marek Rychly <marek.rychly@gmail.com>

# https://github.com/openstack/horizon/releases
ARG HORIZON_VERSION="18.3.1"

ARG HORIZON_GIT="https://github.com/openstack/horizon.git"

ENV \
HORIZON_HOME="/opt/horizon"

RUN true \
# git: to clone the repository (build by tox requires the Git repo, not an extracted archive)
# other deps from (test-)requirements.txt to built the required python modules
&& apk add --no-cache --update git gcc python3-dev musl-dev libffi-dev openssl-dev \
\
# clean up
&& rm -rf /tmp/* /var/tmp/* /var/cache/apk/*

RUN true \
# download the package
&& git clone --depth 1 --single-branch --branch "${HORIZON_VERSION}" "${HORIZON_GIT}" /opt/horizon \
\
# build Python environment
&& sed -i 's|^#!/usr/bin/env python$|#!/usr/bin/env python3|g' ${HORIZON_HOME}/*.py \
&& pip3 install -c "https://releases.openstack.org/constraints/upper/ussuri" -r /opt/horizon/test-requirements.txt -r /opt/horizon/requirements.txt \
\
# clean up
&& rm -rf /tmp/* /var/tmp/* /var/cache/apk/*

COPY scripts /

RUN true \
# make the scripts executable
&& chmod 755 /*.sh \
\
## configure
&& CFG="${HORIZON_HOME}/openstack_dashboard/local/local_settings.py" \
&& cp "${CFG}.example" "${CFG}" \
# disable debug (to server static pages directly, runserver must be executed with arg --insecure)
&& echo "DEBUG = False" >> "${CFG}" \
# enable compression to speed up execution
&& echo "COMPRESS_ENABLED = True" >> "${CFG}" \
&& echo "COMPRESS_OFFLINE = True" >> "${CFG}" \
# using "tox -e runserver" for developments
&& echo "SESSION_ENGINE = 'django.contrib.sessions.backends.signed_cookies'" >> "${CFG}" \
# allow public access
&& echo "ALLOWED_HOSTS = ['*', ]" >> "${CFG}" \
# set Keystone URL from env
&& echo "OPENSTACK_KEYSTONE_URL = os.environ.get('KEYSTONE_URL', OPENSTACK_KEYSTONE_URL)" >> "${CFG}" \
&& echo "OPENSTACK_HOST = None" >> "${CFG}" \
# set Keystone version from env
&& echo "OPENSTACK_API_VERSIONS = {'identity': int(os.environ.get('IDENTITY_API_VERSION', '3')) }" >> "${CFG}" \
\
&& true

ENTRYPOINT ["/entrypoint.sh"]

HEALTHCHECK CMD /healthcheck.sh
